## Test environments

* local Linux Mint 21 (Ubuntu 22.04), R 4.2.3
* Microsoft Windows Server 2019, R-release, R-oldrel (r-universe)
* macOS R-release, R-oldrel (r-universe)
* windows-x86_64-devel, fedora-clang-devel, ubuntu-gcc-release, fedora-clang-devel, ubuntu-gcc-release, windows-x86_64-devel (r-hub)

## R CMD check results

0 errors | 0 warnings | 0 notes
